﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[AddComponentMenu("Devika Learning/General/On Collision Event")]
public class OnCollisionEvent : MonoBehaviour {
	
	public List<GameObject> triggerObjects;
	public LayerMask layers;
	
	public UnityEvent onTriggerEnter;
	public UnityEvent onTriggerStay;
	public UnityEvent onTriggerExit;
	
	
	void OnCollisionEnter(Collision other) {
		
		for(int i = 0; i < triggerObjects.Count; i++)
		{
			if (other.gameObject == triggerObjects[i])
			{
				onTriggerEnter.Invoke();
				//Debug.Log("OnTriggerEnter Invoke");
			}
			
		}
	}
	
	void OnCollisionStay(Collision other) {
		
		for(int i = 0; i < triggerObjects.Count; i++)
		{
			if (other.gameObject == triggerObjects[i])
			{
				onTriggerStay.Invoke();
				//Debug.Log("OnTriggerStay Invoke");
			}
			
		}
	}
	
	void OnCollisionExit(Collision other) {
		
		for(int i = 0; i < triggerObjects.Count; i++)
		{
			if (other.gameObject == triggerObjects[i])
			{
				onTriggerExit.Invoke();
				//Debug.Log("OnTriggerEXit Invoke");
			}
			
		}
	}
	
}
