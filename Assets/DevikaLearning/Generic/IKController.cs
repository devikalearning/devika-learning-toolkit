﻿using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(Animator))] 

public class IKController : MonoBehaviour {
	
	protected Animator animator;
	
	public bool ikActive = false;
	public Transform rightHandObj = null;
	public Transform leftHandObj = null;
	public Transform rightFootObj = null;
	public Transform leftFootObj = null;
	public Transform lookObj = null;
	
	public Vector3 weight = Vector3.zero, maxWeight = new Vector3(0.1f,0.5f,1f);
	
	public void ResetWeight(float time)
	{
		weight = Vector3.zero;//new Vector3(0,0,0);
	}
	
	void Start () 
	{
		animator = GetComponent<Animator>();
	}
	
    //a callback for calculating IK
	void OnAnimatorIK()
	{
		if(animator) {
			
            //if the IK is active, set the position and rotation directly to the goal. 
			if(ikActive) {
				
				//if(weight.magnitude < maxWeight.magnitude)
				//{
				//	weight += maxWeight * Time.deltaTime;
				//}
				
                // Set the look target position, if one has been assigned
				if(lookObj != null) {
					animator.SetLookAtWeight(1,maxWeight.x, maxWeight.y, maxWeight.z);
					animator.SetLookAtPosition(lookObj.position);
				}    
				
                // Set the right hand target position and rotation, if one has been assigned
				if(rightHandObj != null) {
					animator.SetIKPositionWeight(AvatarIKGoal.RightHand,1);
					animator.SetIKRotationWeight(AvatarIKGoal.RightHand,1);  
					animator.SetIKPosition(AvatarIKGoal.RightHand,rightHandObj.position);
					animator.SetIKRotation(AvatarIKGoal.RightHand,rightHandObj.rotation);
				}    
				
				
                // Set the right hand target position and rotation, if one has been assigned
				if(leftHandObj != null) {
					animator.SetIKPositionWeight(AvatarIKGoal.LeftHand,1);
					animator.SetIKRotationWeight(AvatarIKGoal.LeftHand,1);  
					animator.SetIKPosition(AvatarIKGoal.LeftHand,leftHandObj.position);
					animator.SetIKRotation(AvatarIKGoal.LeftHand,leftHandObj.rotation);
				}    
				
                // Set the right hand target position and rotation, if one has been assigned
				if(rightFootObj != null) {
					animator.SetIKPositionWeight(AvatarIKGoal.RightFoot,1);
					animator.SetIKRotationWeight(AvatarIKGoal.RightFoot,1);  
					animator.SetIKPosition(AvatarIKGoal.RightFoot,rightFootObj.position);
					animator.SetIKRotation(AvatarIKGoal.RightFoot,rightFootObj.rotation);
				}    
				
				
                // Set the right hand target position and rotation, if one has been assigned
				if(leftFootObj != null) {
					animator.SetIKPositionWeight(AvatarIKGoal.LeftFoot,1);
					animator.SetIKRotationWeight(AvatarIKGoal.LeftFoot,1);  
					animator.SetIKPosition(AvatarIKGoal.LeftFoot,leftFootObj.position);
					animator.SetIKRotation(AvatarIKGoal.LeftFoot,leftFootObj.rotation);
				}        
				
			}
			
            //if the IK is not active, set the position and rotation of the hand and head back to the original position
			else {          
				animator.SetIKPositionWeight(AvatarIKGoal.RightHand,0);
				animator.SetIKRotationWeight(AvatarIKGoal.LeftHand,0); 
				animator.SetLookAtWeight(0);
			}
		}
	}    
}