﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Devika Learning/Move/Move Towards")]
public class MoveTowards : MonoBehaviour {

	Vector3 target;
	public float speed = 1;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (transform.position != target) {
			transform.position = Vector3.MoveTowards (transform.position, target, speed);
		}
	}

	public void Lerp(Vector3 _target)
	{
		target = _target; 
	}

	public void Slerp(Vector3 _target)
	{
		target = _target;
	}

	public void Move(Vector3 _target)
	{
		target = _target;
	}

}
