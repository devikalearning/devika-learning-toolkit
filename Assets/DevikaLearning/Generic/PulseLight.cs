﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulseLight : MonoBehaviour {
	
	public float speed = 5;
	public float maxIntensity = 5, minIntensity = 1;
	
	Light light;
	
	float counter, intensity;
	
	// Use this for initialization
	void Start () {
		light = this.GetComponent<Light>();// as ;
	}
	
	// Update is called once per frame
	void Update () {
		
		counter += Time.deltaTime;
		
		intensity = (Mathf.Sin(counter*speed)/2)*(maxIntensity-minIntensity) + 0.5f + minIntensity;
		
		//Debug.Log(renderer);
		light.intensity = intensity;
		
	}
	
	public void Reset()
	{
		counter = 0;
	}
}
