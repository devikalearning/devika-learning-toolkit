﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Devika Learning/General/Look At")]
public class LookAt : MonoBehaviour {
	
	public Transform target;
	public bool flipX,flipY,flipZ;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		transform.LookAt(target);
		
		if(flipY)
		{
			transform.forward = -transform.forward;
		}
		
		if(flipX)
		{
			transform.up = -transform.up;
		}
		
		if(flipZ)
		{
			transform.right = -transform.right;
		}
		
	}
}
