﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class counter : MonoBehaviour {
	
	int count = 0;
	public List<Text> countUI;
	public List<Text> timeUI;
	
	public bool recordTimes;
	//public bool saveToFile;
	
	[HideInInspector]
	public List<float> timesPressed;
	
	void Start()
	{
		if(recordTimes)
			ClearSavedTimes();
		count = 0;
		UpdateUI();
	}
	// Use this for initialization
	public int GetCount () {
		return count;
	}
	
	// Update is called once per frame
	public void Increase () {
		
		count++;
		
		if(recordTimes)
		{
			timesPressed.Add(Time.time);
			
			//save to file
		}
		
		UpdateUI();
	}
	
	// Update is called once per frame
	public void Reset () {
		count = 0;
		UpdateUI();
	}
	
	public void ResetAndClear () {
		count = 0;
		
		UpdateUI();
		
		if(recordTimes)
			ClearSavedTimes();
	}
	
	public void ClearSavedTimes()
	{
		timesPressed = new List<float>();
	}
	
	void UpdateUI()
	{
		/*int i;
		for(i = 0; i < countUI.Count; i++)
		{
			countUI[i].text = count.ToString();
		}
		
		
		i = 0;
		timeUI[i].text = "";
		for(int x = 0; x < timesPressed.Count; x++)
		{
			if(!MeetingManager.instance.TimeInBias(timesPressed[x]))
			{
				timeUI[i].text += "X ";
			}
			timeUI[i].text += (((float)((int)(timesPressed[x]*10.0f))) / 10.0f).ToString() + '\n';
			
			
			if(x % 8 == 7)
			{
				i++;
				
				if(i < timeUI.Count)
				{
					timeUI[i].text = "";
				}else
				{
					x = timesPressed.Count;
				}
				
			}
				
		}*/
		
		
	}
}
