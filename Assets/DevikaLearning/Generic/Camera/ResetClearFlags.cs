﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Devika Learning/Camera/Reset Clear Flags")]
public class ResetClearFlags : MonoBehaviour {
	
	Camera camera;
	
	public bool onStart;
	// Use this for initialization
	void Start () {
		
		if(onStart)
			Reset();
	}
	
	// Update is called once per frame
	public void Reset () {
		
		camera = this.GetComponent<Camera>();
		camera.clearFlags = CameraClearFlags.SolidColor;
		camera.backgroundColor = Color.white;
	}
}
