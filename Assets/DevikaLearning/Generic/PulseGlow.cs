﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulseGlow : MonoBehaviour {
	
	public float speed = 5;
	
	Renderer renderer;
	
	float counter, intensity;
	
	// Use this for initialization
	void Start () {
		renderer = this.GetComponent<Renderer>();// as ;
	}
	
	// Update is called once per frame
	void Update () {
		
		counter += Time.deltaTime;
		
		intensity = Mathf.Sin(counter*speed)/2 + 0.5f;
		
		//Debug.Log(renderer);
		renderer.material.SetColor ("_EmissionColor", Color.Lerp(Color.black,Color.red,intensity));//DynamicGI.SetEmissive(renderer, new Color(1f, 0.1f, 0.5f, 1.0f) * intensity);
		DynamicGI.UpdateMaterials(renderer);
		DynamicGI.UpdateEnvironment();
				
	}
}
