﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DevikaLearning {
	
	[AddComponentMenu("Devika Learning/Game Object Controller")]
	public class GameObjectController : MonoBehaviour {
		
		[System.Serializable]
		public class GameObjectGroup{
			
			public string name;

			public List<GameObject> gameObjects;
			
			public void SetActive(bool active)
			{
				for(int i = 0; i < gameObjects.Count; i++)
				{
					gameObjects[i].SetActive(active);
				}
			}
		}
		
		public List<GameObjectGroup> GameObjectGroups = new List<GameObjectGroup>();
		
		int currentGroupId = 0;
		
		// Use this for initialization
		void Start () {
			
			TurnOffAll();
			
			if(GameObjectGroups.Count > 0)
				GameObjectGroups[currentGroupId].SetActive(true);
		}
		
		void TurnOffAll()
		{
			for(int i = 0; i < GameObjectGroups.Count; i++)
			{
				GameObjectGroups[i].SetActive(false);
			}
		}
		
		public void NextGroup()
		{
			TurnOffAll();
			
			currentGroupId++;
			
			GameObjectGroups[currentGroupId % GameObjectGroups.Count].SetActive(true);
		}
		
		public void TurnOnGroup(int id)
		{
			TurnOffAll();
			
			currentGroupId = id;
			
			GameObjectGroups[currentGroupId % GameObjectGroups.Count].SetActive(true);
		}
		
		public void TurnOnGroup(string name)
		{
			int id = FindGroupID(name);
			
			if(id != -1)
				TurnOnGroup(id);
		}
		
		int FindGroupID(string name)
		{
			for(int i = 0; i < GameObjectGroups.Count; i++)
			{
				if (GameObjectGroups[i].name == name)
					return i;
			}
			
			return -1;
		}
	}
	
}