﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointsManager : MonoBehaviour {
	
	int points;
	
	public List<Text> PointsDisplay = new List<Text>();
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void UpdateDisplay()
	{
		for(int i = 0; i < PointsDisplay.Count; i++)
		{
			PointsDisplay[i].text = points.ToString();
		}
	}
	
	public void IncreasePoints()
	{
		points++;
		
		UpdateDisplay();
	}
}
