﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CollectableEvent : MonoBehaviour {
	
	public UnityEvent Event;
	
	public string tag = "Player";
	
	public void Collect(GameObject target)
	{
		if(tag != target.tag)
			return;
		
		Event.Invoke();
	}
	
	void OnTriggerEnter(Collider other)
	{
		Collect(other.gameObject);
	}
	
	void OnCollisionEnter(Collision col)
	{
		Collect(col.gameObject);
	}
}
