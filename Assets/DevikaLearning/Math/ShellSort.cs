﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellSort : MonoBehaviour {

	/**
 * Shell sort - sort with diminishing increment (descending)
 * @param array to be sorted
 * @return sorted array
 */
	public static int[] shellSort(int[] array) {
		int gap = array.Length / 2;
		while (gap > 0) {
			for (int i = 0; i < array.Length - gap; i++) { //modified insertion sort
				int j = i + gap;
				int tmp = array[j];
				while (j >= gap && tmp > array[j - gap]) {
					array[j] = array[j - gap];
					j -= gap;
				}
				array[j] = tmp;
			}
			if (gap == 2) { //change the gap size
				gap = 1;
			} else {
				gap = (int)(gap / 2.2f);
			}
		}
		return array;
	}
}
