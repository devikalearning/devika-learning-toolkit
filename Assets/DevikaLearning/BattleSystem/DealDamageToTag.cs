﻿using UnityEngine;
using System.Collections;

namespace DevikaLearning
{
	
	[AddComponentMenu("Devika Learning/BattleSystem/Deal Damage To Tag")] //this addes the compent to the Add Component Menu
	public class DealDamageToTag : MonoBehaviour {
	
		//the amount of damage done to health
		public float damage;
		
		public string tag;
	
		void OnTriggerEnter(Collider other)
		{
			
			if(other.tag != tag)
				return;
			
			//Get the health component of other object
			Health health = other.GetComponent<Health> ();
	
			//If it has a health component
			if (health) {
	
				//take away damage from health
				health.health -= damage;
			}
		}
	
		void OnCollisionEnter(Collision col)
		{
			
			if(col.gameObject.tag != tag)
				return;
			
			//Get the health component of other object
			Health health = col.gameObject.GetComponent<Health> ();
	
			//If it has a health component
			if (health) {
	
				//take away damage from health
				health.health -= damage;
			}
		}
	}
}
