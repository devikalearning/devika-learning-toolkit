﻿using UnityEngine;
using System.Collections;

namespace DevikaLearning
{
	
	public enum MouseButton{
		Left = 0,
		Right = 1,
		Middle = 2,
		None = -1
	}
	
	[AddComponentMenu("Devika Learning/BattleSystem/Attack")] //this addes the compent to the Add Component Menu
	public class Attack : MonoBehaviour {
		
		public KeyCode attackKey;
		
		public MouseButton mouseButton = MouseButton.None;
		
		//the amount of damage done to health
		public float damage;
		
		public string tag;
		
			
	
		void OnTriggerEnter(Collider other)
		{
			if(attackKey != null)
			{
				if(!Input.GetKeyDown(attackKey))
				{
					return;
				}
			}
			
			if(mouseButton != MouseButton.None)
			{
				if(!Input.GetMouseButtonDown((int)mouseButton))
				{
					return;
				}
			}
			
			if(other.tag != tag && tag != null)
				return;
			
			AttackTarget(other.gameObject);
		}
	
		void OnCollisionEnter(Collision col)
		{
			if(attackKey != null)
			{
				if(!Input.GetKeyDown(attackKey))
				{
					return;
				}
			}
			
			if(mouseButton != MouseButton.None)
			{
				if(!Input.GetMouseButtonDown((int)mouseButton))
				{
					return;
				}
			}
			
			if(col.gameObject.tag != tag && tag != null)
				return;
			
			AttackTarget(col.gameObject);
		}
		
		public void AttackTarget(GameObject target)
		{
			
			//Get the health component of other object
			Health health = target.GetComponent<Health> ();
			
			//If it has a health component
			if (health) {
				
				//take away damage from health
				health.health -= damage;
			}
		}
	}
}
