﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Devika Learning/VR Tools/Teleport To Object")]
public class TeleportToObject : MonoBehaviour {
	
	public Transform target;
	public Transform CameraRig, Head;
	
	public bool OnlyOnce = true;
	bool teleported = false;
	
	
	public void Teleport()
	{
		if(OnlyOnce && !teleported)
		{
			Vector3 targetPos = target.transform.position - Head.transform.position;
			CameraRig.transform.position += new Vector3(targetPos.x, 0, targetPos.z);
			//Head.transform.position = new Vector3(targetPos.x, Head.transform.position.y, targetPos.z);
			teleported = true;
		}
	}
	
}
