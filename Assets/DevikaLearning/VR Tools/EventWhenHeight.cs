﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[AddComponentMenu("Devika Learning/VR Tools/Event When Height")]
public class EventWhenHeight : MonoBehaviour {


    public float height;
    public float buffer;
    public GameObject target;
    public UnityEvent whenHigher;
    public UnityEvent whenlower;
    float originalheight;

    public bool constantEventCall = false;

    bool higher = true;

    // Use this for initialization
    void Start()
    {
        originalheight = height;
    }

    // Update is called once per frame
    void Update()
    {

        if (target.transform.position.y > height  && (higher == false || constantEventCall))
        {
            height = originalheight + buffer;
            higher = true;
            whenHigher.Invoke();
        }
        else if  ( higher == true || constantEventCall)
        {
            height = originalheight - buffer;
            higher = false;
            whenlower.Invoke();
        }

    }
}