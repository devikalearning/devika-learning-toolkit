﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Devika Learning/VR Tools/Set Head Height")]
public class SetHeadHeight : MonoBehaviour {
	
	public Transform Head;
	public Transform Camera;
	public KeyCode key;
	
	bool set = false;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if(Input.GetKeyDown(key))
		{
			transform.position += Head.position - Camera.position;
		}	
	}
	
	public void SetHeightOnce()
	{
		if (set)
			return;
		
		set = true;
		
		transform.position += Head.position - Camera.position;
	}
}
