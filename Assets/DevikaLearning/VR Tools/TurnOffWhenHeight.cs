﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[AddComponentMenu("Devika Learning/VR Tools/Turn Off When Height")]
public class TurnOffWhenHeight : MonoBehaviour {
	
	public float height;
	public GameObject target;
	
	public bool greaterThan;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if(greaterThan)
		{
			if(target.transform.position.y > height)
			{
				target.gameObject.SetActive(false);
			}
			else
			{
				target.gameObject.SetActive(true);
			}
		}else
		{
			if(target.transform.position.y < height)
			{
				target.gameObject.SetActive(false);
			}
			else
			{
				target.gameObject.SetActive(true);
			}
		}
		
	}
}
